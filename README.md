# Welcome

Sitecore Automation Project is place where I would like to develop Powershell modules used for automating tasks connected with Sitecore Experience Platform.

[Setup Sitecore Environment](https://bitbucket.org/sitecoreautomationteam/sitecore-automation/wiki/sitecore-environment)

A few simple functions to processing a Sitecore enable/disable spreadsheet.

[Sitecore Sites](https://bitbucket.org/sitecoreautomationteam/sitecore-automation/wiki/sitecore-sites)

Functions to automate a tasks base on Site Definition.

[Sitecore Config](https://bitbucket.org/sitecoreautomationteam/sitecore-automation/wiki/sitecore-config)

Functions to automate changes in web.config and sitecore configuration files