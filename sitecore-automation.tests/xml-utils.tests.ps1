﻿$module = Join-Path (Split-Path -parent $MyInvocation.MyCommand.Path).Replace(".tests","") -ChildPath "sitecore-automation.psd1"

Import-Module $module -Verbose -Force

Describe "xml-utils" {
	Context "Exists" {
		It "Runs" {
			$path = 'F:\GIT\SitecorePowershellModules\sitecore-automation.tests\web.test.xml'
			
			#New-IISHandler -Verbose -XmlPath $path -Name "CaptchaImage" -Path 'CaptchaImage.axd' -Type "Sitecore.Form.Core.Pipeline.RequestProcessor.CaptchaResolver, Sitecore.Forms.Core" -Verb "*"
			New-IISHandler -Verbose -Comment "WFFM handler" -XmlPath $path -Name "CaptchaAudio" -Path 'CaptchaAudio.axd' -Type "Sitecore.Form.Core.Pipeline.RequestProcessor.CaptchaResolver, Sitecore.Forms.Core" -Verb "*"
			
			Remove-IISHandler -XmlPath $path -Name "CaptchaAudio" -Verbose
			
			#Disable-IISHandler -XmlPath $path -Name "WebDAVRoot" -Verbose
		}
	}
}



Describe "xml-utils" {
	Context "Exists" {
		It "Runs" {
			$path = 'F:\GIT\SitecorePowershellModules\sitecore-automation.tests\web.test.xml'

			#New-SettingsPatch -XmlPath $path -Name "Xdb.Enabled" -Value "false" -Comment "We do not use MongoDB" -Verbose
		}
	}
}		

Describe "sitecore-patch-file" {
	Context "Create" {
		It "Runs" {
			$path = 'F:\GIT\SitecorePowershellModules\sitecore-automation.tests\settings.xml'

			New-SettingsFile -XmlPath $path -Comment "This settings file was created by Powershell"

			New-SettingsPatch -XmlPath $path -Name "Xdb.Enabled" -Value "false" ` 
			-Comment "We do not use MongoDB on local environment" -Verbose
		}
	}
}	