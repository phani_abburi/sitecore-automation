﻿$module = Join-Path (Split-Path -parent $MyInvocation.MyCommand.Path).Replace(".tests","") -ChildPath "sitecore-automation.psd1"

Test-ModuleManifest -Path $module -Verbose
Import-Module $module -Verbose -Force
#$env:Temp = "C:\Temp"

Describe "sitecore-hosts-tests" {

	BeforeEach {
		$hostsFile = "TestDrive:\hosts"
		Set-Content $hostsFile -value "# Sample hosts file"
		Add-Content $hostsFile -value "127.0.0.1 localhost #sample entry"
	}
	AfterEach {
		Remove-Item "TestDrive:\hosts"
	}
	
	Context "New Host" {
		It "Should Be Added" {
			
			'TestDrive:\hosts' | Should Not Contain 'sitecore.doc'

			$hostsFile = Join-Path $TestDrive -ChildPath 'hosts'
			New-SitecoreHostsEntry -IpAddress 127.0.0.2 -HostName 'sitecore.doc' -Path $hostsFile
			
			'TestDrive:\hosts' | Should Contain 'sitecore.doc'
		}
	}

	Context "Existing Host" {
		It "Should Be Removed" {
				
			'TestDrive:\hosts' | Should Contain 'localhost'
			
			$hostsFile = Join-Path $TestDrive -ChildPath 'hosts'
			Remove-SitecoreHostsEntry -HostName 'localhost' -Path $hostsFile
			
			'TestDrive:\hosts' | Should Not Contain 'localhost'
		}
	}
}