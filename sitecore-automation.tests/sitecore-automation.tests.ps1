﻿#
# This is a PowerShell Unit Test file.
# You need a unit test framework such as Pester to run PowerShell Unit tests. 
# You can download Pester from http://go.microsoft.com/fwlink/?LinkID=534084
#

# This crazy code builds path to sitecore automation module and assume that tests are in module-name.tests folder.
# 1 - $MyInvocation.MyCommand.Path points to this file
# 2 - Split-Path -Parent gets path to 'sitecore-automations.tests' folder
$module = Join-Path (Split-Path -parent $MyInvocation.MyCommand.Path).Replace(".tests","") -ChildPath "sitecore-automation.psd1"

Import-Module $module -Verbose -Force


Describe "Combine-Paths" {
    It "Combine three paths" {
        $path = Combine-Path 'a' 'b' 'c'
        $path | Should Be "a\b\c"
    }

    It "Combine two paths" {
        $path = Combine-Path 'a' 'b'
        $path | Should Be "a\b\"
    }
	 
    It "Combine one path"  {
		#Two parameters are mandatory
	    { Combine-Path 'a' } | Should Throw
    }
}


Describe "Switch-SitecoreConfigFile" {

	$file_to_enable = "TestDrive:\file-to-enable.config.disabled"
	$file_to_disable = "TestDrive:\file-to-disable.config"
	$example_to_enable = "TestDrive:\example-to-enable.config.example"

	$webPath =  $TestDrive

	Context "Configuration file" {
		It "Should be enabled" {
			$args = @{
				"ConfigPath" = Join-Path $TestDrive -ChildPath 'file-to-enable.config.disabled';
				"Enable" = $true;
				"ProcessExample" = $false
			}
			
			$args | Switch-SitecoreConfigFile

			"TestDrive:\file-to-enable.config.disabled" | Should Not Exist
			"TestDrive:\file-to-enable.config" | Should Exist
		}

		It "Is disabled and should not be changed" {
			$args = @{
				"ConfigPath" = Join-Path $TestDrive -ChildPath 'file-to-enable.config.disabled';
				"Enable" = $false;
				"ProcessExample" = $false
			}
			
			$args | Switch-SitecoreConfigFile

			"TestDrive:\file-to-enable.config.disabled" | Should  Exist
			"TestDrive:\file-to-enable.config" | Should Not Exist
		}

		It "Should be disabled" {
			$args = @{
				"ConfigPath" = Join-Path $TestDrive -ChildPath 'file-to-disable.config';
				"Enable" = $false;
				"ProcessExample" = $false
			}
			
			$args | Switch-SitecoreConfigFile

			"TestDrive:\file-to-disable.config" | Should Not Exist
			"TestDrive:\file-to-disable.config.disabled" | Should Exist
		}

		It "Is enabled and should not be changed" {
			$args = @{
				"ConfigPath" = Join-Path $TestDrive -ChildPath 'file-to-disable.config';
				"Enable" = $true;
				"ProcessExample" = $false
			}
			
			$args | Switch-SitecoreConfigFile

			"TestDrive:\file-to-disable.config" | Should Exist
			"TestDrive:\file-to-disable.config.disabled" | Should Not Exist
		}
	}

	Context "Example file" {
		It "Should be enabled" {
			$args = @{
				"ConfigPath" = Join-Path $TestDrive -ChildPath 'example-to-enable.config.example';
				"Enable" = $true;
				"ProcessExample" = $true
			}
			
			$args | Switch-SitecoreConfigFile

			"TestDrive:\example-to-enable.config.example" | Should Not Exist
			"TestDrive:\example-to-enable.config" | Should Exist
		}
	}

	BeforeEach {
		md "TestDrive:\file-to-enable.config.disabled"
		md "TestDrive:\file-to-disable.config"
		md "TestDrive:\example-to-enable.config.example"

		Remove-Item "TestDrive:\file-to-enable.config"
		Remove-Item "TestDrive:\file-to-disable.config.disabled"
		Remove-Item "TestDrive:\example-to-enable.config"
	}
}


Describe "Set-SitecoreEnvironment" {

	$csvPath = "TestDrive:\test.csv"
	$webPath =  $TestDrive
	#create CSV header
	Set-Content $csvPath -value "ProductName,FilePath,ConfigFileName,Type,SearchProviderUsed,ContentDelivery,ContentManagement,Processing,CMProcessing,Reporting"
	
	Add-Content $csvPath -Value "Platform,\website\,Web.config,config,,Enable,Enable,Enable,Enable,Enable"
	Add-Content $csvPath -Value "SPEAK,\website\App_Config\Include,Sitecore.Speak.Mvc.config,config,,Disable,Enable,Disable,Enable,Disable"

	Add-Content $csvPath -Value "Platform - Search,\website\App_Config\Include,Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config.example,example,Solr is used,Enable,Enable,Enable,Enable,Enable"
	Add-Content $csvPath -Value "Marketing Foundation,\website\App_Config\Include,Sitecore.Marketing.Solr.Index.Master.config.disabled,config,Solr is used,Disable,Enable,Disable,Enable,Disable"
	
	Context "Check Files" {
		It "test.csv should exist" {
			$csvPath | Should Exist
		}
		It "Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config.example should exist" {
			"TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config.example" | Should Exist
		}
		It "Sitecore.Marketing.Solr.Index.Master.config.disabled should exist" {
			"TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled"| Should Exist
		}
	}

	#Context "Read CSV file" {
	#	It "Should return proper object" { 
	#	$object = Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Verbose
	#	$object.Count | Should Be 4
		
	#	$object[0].ProductName | Should Be 'Platform'	
 #       $object[0].FilePath | Should Be '\website\'
	#	}
	#}



	Context "Test Search Parameters" {
		#Search parameter should be from [ValidateSet('Solr','Lucene')]
		It "Coveo as Search" {
			{ Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Search "Coveo" } | Should Throw
		}
		
		It "Empty as Search" {
			{ Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Search "" } | Should Throw
		}
	}

	Context "Test Environment Parameters" {
		It "Unknown as Environment" {
			{ Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath  -Environment "unknown"  } | Should Throw
		}
		
		It "Empty as Environment" {
			{ Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Environment ""  } | Should Throw
		}
	}

	Context "Configure Solr Search" {
		It "For ContentDelivery" {
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Search "Solr" -Environment "ContentDelivery"  -Verbose
			
			"TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Exist
			"TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config" | Should Not Exist
			"TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config" | Should Exist
		}
		
		It "For ContentManagement" {
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Search "Solr" -Environment "ContentManagement"  -Verbose
			
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Be $false
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config" | Should Be $true	
			"TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config" | Should Exist
		}

		It "For Processing" {
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Search "Solr" -Environment "Processing"  -Verbose
			
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Be $true
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config" | Should Be $false
			"TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config" | Should Exist	
		}

		It "For CMProcessing" {
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Search "Solr" -Environment "CMProcessing"  -Verbose
			
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Be $false
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config" | Should Be $true	
			"TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config" | Should Exist
		}

		It "For Reporting" {
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Search "Solr" -Environment "Reporting"  -Verbose
			
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Be $true
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config" | Should Be $false	
			"TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config" | Should Exist
		}
	}

	Context "Configure Environment" {
		

		It "For ContentDelivery" {
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Environment "ContentDelivery"  -Verbose

			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Be $true
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config" | Should Be $false

			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Speak.Mvc.config.disabled" | Should Be $true
		}
		
		It "For ContentManagement" {
			
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Environment "ContentManagement"  -Verbose
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Be $true
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config" | Should Be $false

			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Speak.Mvc.config.disabled" | Should Be $false
		}

		It "For Processing" {
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Environment "Processing"  -Verbose

			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Be $true
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Speak.Mvc.config.disabled" | Should Be $true
		}

		It "For CMProcessing" {
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Environment "CMProcessing"  -Verbose

			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Be $true
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Speak.Mvc.config.disabled" | Should Be $false
		}

		It "For Reporting" {
			Set-SitecoreEnvironment -WebPath $webPath -CsvPath $csvPath -Environment "Reporting"  -Verbose

			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled" | Should Be $true
			Test-Path "TestDrive:\website\App_Config\Include\Sitecore.Speak.Mvc.config.disabled" | Should Be $true
		}
		
	}

	BeforeEach {
		# creates default files expected by tests
        md "TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config.example"
		md "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config.disabled"
		md "TestDrive:\website\App_Config\Include\Sitecore.Speak.Mvc.config"
		md "TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config.example"

		# clean up modified files
		Remove-Item "TestDrive:\website\App_Config\Include\Sitecore.Marketing.Solr.Index.Master.config"
		Remove-Item "TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config"
		Remove-Item "TestDrive:\website\App_Config\Include\Sitecore.Speak.Mvc.config.disabled"
		Remove-Item "TestDrive:\website\App_Config\Include\Sitecore.ContentSearch.Solr.DefaultIndexConfiguration.config"
    }

	
}



