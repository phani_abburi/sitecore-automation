$projectFolder = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = Join-Path $projectFolder -ChildPath "sitecore-automation"
$manifestPath = Join-Path $modulePath -ChildPath "sitecore-automation.psd1"

Write-Output "Path: $manifestPath"

Update-ModuleManifest -Path $manifestPath -Tags @('Sitecore', 'Automation', 'SPE')