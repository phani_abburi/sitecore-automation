$projectFolder = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = Join-Path $projectFolder -ChildPath "sitecore-automation"

Write-Output $modulePath 

$userModules = Join-Path $env:USERPROFILE -ChildPath "Documents\WindowsPowerShell\Modules\sitecore-automation"

if( -not (Test-Path -Path $userModules))
{
	md $userModules
	md "$userModules\assets"
}


Write-Output "Path: $userModules"

Copy-Item -Recurse -Path $modulePath\* -Destination "$userModules" -Include *.psd1, *.psm1, *.ps1 -Force -Verbose
Copy-Item -Recurse -Path $modulePath\assets\* -Destination "$userModules\assets" -Include *.csv -Force -Verbose

#$modulesPath = [Environment]::GetEnvironmentVariable("PSModulePath", "Machine")

#if($modulesPath -notlike "*$($modulePath)*") {
#   [Environment]::SetEnvironmentVariable("PSModulePath", $env:PSModulePath , "Machine")
#    $env:PSModulePath = $env:PSModulePath
#    Write-Output "$modulePath is registered"
#}
#else
#{
#    Write-Output "$modulePath is already registered"
#}