#
# sitecore_bindings.ps1
#



function Add-Binding($SiteName, $HostHeader )
{
    $bindings = Get-WebBinding -Name $SiteName
    
    if( $bindings.bindingInformation -match $HostHeader )
    {
        Write-Verbose "A binding '$HostHeader' already exist on site '$SiteName'"
    }
    else
    {
        try
        {
            Write-Verbose "Adds a binding '$HostHeader' to an IIS '$SiteName' site"

            New-WebBinding -Name $SiteName -IPAddress "*" -Port 80 -HostHeader $HostHeader;
        }
        catch
        {
            Write-Warning "Adds a binding '$HostHeader' failed - $($_.Exception.Message)"
        }
    }
}

function Add-SitecoreBindings
{
    [cmdletbinding(SupportsShouldProcess=$true)]
     Param(
        [ValidateNotNullOrEmpty()] 
        $Hosts, 
        [ValidateNotNullOrEmpty()] 
        [string]$WebSite,

        [switch]$www
     )

        #region ValidatePreconditions
        if( -not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
        {
            Write-Warning "Add bindings requires Administrator rights"
            return
        }
      
        if( -not (Test-Path -Path (Join-Path 'IIS:\Sites' -ChildPath $WebSite ) ) )
        {
            Write-Warning "Site '$WebSite' does not exist."
            return
        }
        #endregion

        foreach($hostName in $Hosts)
        {
            Add-Binding -Site $WebSite -HostHeader $hostName
             
            if( ($www -eq $true) -and ($hostName.StartsWith("www") -eq $false) )
            {
                Add-Binding -Site $WebSite -HostHeader "www.$hostName"
            }
        }
}


function Remove-SitecoreBindings
{
    [cmdletbinding(SupportsShouldProcess=$true)]
     Param(
        [ValidateNotNullOrEmpty()] 
        $Hosts, 
        [ValidateNotNullOrEmpty()] 
        [string]$WebSite
     )
	
        #region ValidatePreconditions
        if( -not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
        {
            Write-Warning "Add bindings requires Administrator rights"
            return
        }
      
        if( -not (Test-Path -Path (Join-Path "IIS:\Sites" -ChildPath $WebSite ) ) )
        {
            Write-Warning "Site '$WebSite' does not exist."
            return
        }
        #endregion

        foreach($hostName in $Hosts)
        {
            $bindings = Get-WebBinding -Name $WebSite;

            if( $bindings.bindingInformation -match $hostName )
            {
                Write-Verbose "Removes a binding '$hostName' from an IIS '$webSite' site."
                Remove-WebBinding -Name $WebSite -IPAddress "*" -Port 80 -HostHeader  $hostName
            }
            
            if( $bindings.bindingInformation -match "www.$hostName" )
            {
                Write-Verbose "Removes a binding 'www.$hostName' from an IIS '$webSite' site."
                Remove-WebBinding -Name $WebSite -IPAddress "*" -Port 80 -HostHeader  "www.$hostName"
            }
        }
	
}

Export-ModuleMember -Function Add-SitecoreBindings
Export-ModuleMember -Function Remove-SitecoreBindings