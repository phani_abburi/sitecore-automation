#
# sitecore_spe.ps1
#
function Get-DefaultSpeConfig()
{
    # $AppPath is SPE console variable, see Variables on https://doc.sitecorepowershell.com/interfaces.html
    if( $AppPath -ne $null )
    {
        return Join-Path -Path $AppPath -ChildPath "App_Config\Include\Cognifide.PowerShell.config"
    }
}


function Set-UAC4SPE
{
	<#
	.SYNOPSIS
	Sets an User Access Control parameters for Sitecore Powershell Extension

	.DESCRIPTION
	More about Sitecore Powershell Extension security https://doc.sitecorepowershell.com/security.html

	.PARAMETER Token
	An unique string used for the gate token attribute ('Default','Console','ISE','ItemSave')
	
	.PARAMETER Action
	An action to perform when session elevation is triggered (Allow, Block, Password)
	
	.PARAMETER Expiration
	A timespan used to determine the elevated session lifetime (hh:mm:ss)

	.PARAMETER SpeConfig
	A path to Cognifide.PowerShell.config. 
	If this function is used in a SPE console then default parameter should be enough Get-DefaultSpeConfig
	 
	.EXAMPLE
	Set-UAC4SPE -Token Console -Expiration '00:06:00'  

	Set-UAC4SPE -Token Console -Expiration '00:06:00' -Action Allow 
	
	.EXAMPLE
	PowerShell will number them for you when it displays your help text to a user.
#>
    [CmdletBinding(SupportsShouldProcess=$true)]
    Param(
      [parameter(Mandatory=$true)]
      [ValidateSet('Default','Console','ISE','ItemSave')]
      [string]$Token = $null,
      [parameter()]
      [ValidateSet('Block','Password','Allow')]
      [string]$Action = $null,
      [string]$Expiration = $null,
      [string]$SpeConfig = (Get-DefaultSpeConfig)
    )

    Write-Verbose "Set User Account Control in file $SpeConfig"

    [xml]$XmlDocument = Get-Content -Path $SpeConfig

    $xpath = "//configuration/sitecore/powershell/userAccountControl/tokens//token[@name = '$Token' ]"

    $tokenNode = $XmlDocument.SelectSingleNode($xpath) 

    if( $Action -ne '' )
    {
        $tokenNode.Attributes["elevationAction"].Value = $Action

        if ($pscmdlet.ShouldProcess("Set elevationAction to $Action on $SpeConfig"))
        {
            $XmlDocument.Save($SpeConfig)
        }
    }

    if( $Expiration -ne '' )
    {
        $tokenNode.Attributes["expiration"].Value = $Expiration
        if ($pscmdlet.ShouldProcess("Set expiration to $Expiration on $SpeConfig"))
        {
            $XmlDocument.Save($SpeConfig)
        }
    }
}
#
function Get-UAC4SPE
{
	<#
	.SYNOPSIS
	Gets information about settings of passed token.
	
	.DESCRIPTION
	More about Sitecore Powershell Extension security https://doc.sitecorepowershell.com/security.html

	.PARAMETER Token
	An unique string used for the gate token attribute ('Default','Console','ISE','ItemSave')

	.PARAMETER SpeConfig
	A path to Cognifide.PowerShell.config. 
	If this function is used in a SPE console then default parameter should be enough Get-DefaultSpeConfig
	
	.EXAMPLE
	Get-UAC4SPE -Token Console 

	#>
    [CmdletBinding(SupportsShouldProcess=$true)]
    Param(
      [parameter(Mandatory=$true)]
      [ValidateSet('Default','Console','ISE','ItemSave')]
      [string]$Token = $null,

      [string]$SpeConfig = (Get-DefaultSpeConfig)
    )
    
    Write-Verbose "Get User Account Control from file '$SpeConfig'"

    [xml]$XmlDocument = Get-Content -Path $SpeConfig

    $xpath = "//configuration/sitecore/powershell/userAccountControl/tokens//token[@name = '$Token' ]"

    $tokenNode = $XmlDocument.SelectSingleNode($xpath) 
    
    return $tokenNode
}


Export-ModuleMember -Function Get-UAC4SPE
Export-ModuleMember -Function Set-UAC4SPE
