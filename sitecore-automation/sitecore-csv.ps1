<#
.Synopsis
	Processing Config Enable-Disable Sitecore_8.2-160906_RTM.csv file

.DESCRIPTION
	This function is processing csv file, that contains guideline about enable/disable configuration
	files on each enviroment. 

#>
function Set-SitecoreEnvironment {
[CmdletBinding(SupportsShouldProcess=$true)]
    param (
		[parameter(Mandatory=$true)]
        [string]$CsvPath,

        [parameter(Mandatory=$true)]
        [string]$WebPath,
        
        [parameter()]
        [ValidateSet('ContentDelivery','ContentManagement','Processing','CMProcessing','Reporting')]
        [string]$Environment = $null,

        [parameter()]
        [ValidateSet('Solr','Lucene')]
        [string]$Search = $null,

		[switch]$Backup
		
    )
	
	$projectFolder = Split-Path -Parent $MyInvocation.MyCommand.Path

	Write-Verbose $projectFolder

	if($Backup -eq $true)
	{
		$sourceFolder = Join-Path -Path $WebPath -ChildPath "website\App_Config"
		# More about date formating - https://technet.microsoft.com/en-us/library/ee692801.aspx
		$date = Get-Date -Format d-M-yyyy-HH-MM-s
		$backupFile = "website\App_Config-$date.zip"
		$backupPath = Join-Path -Path $WebPath -ChildPath $backupFile
		Zip-Directory -DestinationFileName $backupPath -SourceDirectory $sourceFolder
	}
		
	if( ($Search -ne '') -and ($Environment -ne ''))
	{
		Write-Verbose "Configure $Search for environment: $Environment"
		# Prefilter all configuration files to select only files with values 'Solr is used' or 'Lucene is used'
		$searchProviderPreFilter = {$_.SearchProviderUsed -ne '' -and $_.SearchProviderUsed -ne 'Base' }

		# Set field 'Enable' to true if
		#      configuration file is used by selected search engine
		#      and configuration file should be enabled on selected environment
		$action = @{Name="Enable"; Expression = { ($_.SearchProviderUsed -eq "$Search is used" -and $_.$Environment -eq 'Enable')}}

		# Build full path to configuration file
		$file = @{Name="ConfigPath";Expression = {  Combine-Path $WebPath $_.FilePath $_.ConfigFileName}}

		Import-Csv $CsvPath | Where-Object $searchProviderPreFilter  | Select-Object  $file, $action, @{Name="ProcessExample"; Expression={$true}} | Switch-SitecoreConfigFile 
	}
	elseif( $Environment -ne '' -and $Search -eq '')
	{
		Write-Verbose "Configure environment: $Environment"
		# Prefilter all configuration files to select all files not connected with any search provider
		$searchProviderPreFilter = {$_.SearchProviderUsed -eq '' -or $_.SearchProviderUsed -eq 'Base' }

		# Set field 'Enable' to true if
		#      and configuration file should be enabled on selected environment
		$action = @{Name="Enable"; Expression = { ( $_.$Environment -eq 'Enable')}}

		# Build full path to configuration file
		$file = @{Name="ConfigPath";Expression = {  Combine-Path $WebPath $_.FilePath $_.ConfigFileName}}

		Import-Csv $CsvPath  | Where-Object $searchProviderPreFilter | Select-Object  $file, $action, @{Name="ProcessExample"; Expression={$false}} | Switch-SitecoreConfigFile
	}

}

<#
.Synopsis

	Enable or disable Sitecore configuration file

.DESCRIPTION

	This function is used to enable/disable configuration file. This functionality is realized by change file name extension.
	Enable configuration file means - remove extension '.disabled' 
	Disable configuration file means - add extension '.disabled'

	This function process objects passed by pipeline. Object should have two properties ConfigPath and Enable.
	ConfigPath - this is path to configration file 
	Enable - if set to true file is enabled, otherwise disabled

.EXAMPLE
	
#>
function Switch-SitecoreConfigFile
{
    process
    {
		$VerbosePreference = "Continue"
        $configFile = $_.ConfigPath;
       
	
		Write-Verbose "Processing  $configFile..."

		if( -not (Test-Path -Path $configFile) )
		{
			Write-Warning "File not exist"
			return;
        }

		
		$extension = [System.IO.Path]::GetExtension($configFile)
		$processExample = $_.ProcessExample

		# extension .example should be processed only when search configuration is processed
	    if($processExample -eq $true -and $_.Enable -eq $true -and  $extension -eq '.example')
        {
            $newConfigFile = $configFile -replace "\.example","" 
              
            Write-Verbose "Enable example file  $newConfigFile"

            Rename-Item $configFile -NewName $newConfigFile
        }
        elseif($_.Enable -eq $true -and  $extension -eq '.disabled')
        {
            $newConfigFile = $configFile -replace ".disabled","" 
              
            Write-Verbose "Enable file  $newConfigFile"

            Rename-Item $configFile -NewName $newConfigFile
        }
        elseif($_.Enable -eq $false -and $extension -eq '.config' )
        {
            $newConfigFile = $configFile -replace "\.config",".config.disabled"

            Write-Verbose "Disable file $newConfigFile"

            Rename-Item $configFile -NewName $newConfigFile
        }
        else
        {
            Write-Warning "Change is not necessary"
        }
	}
}

<#

	.EXAMPLE
	Combine-Path 'a' 
	
	.EXAMPLE
	Combine-Path 'a' 'b' 

	.EXAMPLE
	Combine-Path 'a' 'b' 'c'
#>
function Combine-Path
{
    [CmdletBinding(SupportsShouldProcess=$true)]
    param (
        [parameter(Mandatory=$true)]
        [string]$Path1,
        
        [parameter(Mandatory=$true)]
        [string]$Path2,

        [parameter()]
        [string]$Path3 
    )
	
	return Join-Path $Path1 -ChildPath (Join-Path $Path2 -ChildPath $Path3)  
}


Export-ModuleMember -Function Set-SitecoreEnvironment