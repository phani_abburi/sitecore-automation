#
# sitecore_tests.ps1
#
Function Test-WebResponse
{
	[cmdletbinding(SupportsShouldProcess=$true)]
	param(
		[parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]		
        [String]
        $Url,
		[int]
		$Timeout = 10000
    )

	$result = $false
	$uri = ""
	$isUrlCorrect = [System.Uri]::TryCreate($Url, [System.UriKind]::Absolute,[ref] $uri)
	if( $isUrlCorrect -eq $false )
	{
		Write-Warning "Can't create URI from '$Url'"
		return $result
	}
	else
	{
		$sw = [Diagnostics.Stopwatch]::StartNew()

		$HTTP_Request = [System.Net.WebRequest]::Create($uri)

		$HTTP_Request.Timeout = $Timeout

		$HTTP_Response = $null
		try
		{
			$HTTP_Response = $HTTP_Request.GetResponse();

			# We then get the HTTP code as an integer.
			$HTTP_Status = [int]$HTTP_Response.StatusCode
			$sw.Stop()

			If ($HTTP_Status -eq 200) 
			{ 
				Write-Verbose "Test SUCCESS for $uri - $($sw.Elapsed)"
				$result = $true
			}
			Else 
			{
				Write-Verbose "Test FAIL for $uri. Response: $HTTP_Status"
			}
		}
		catch
		{
			Write-Warning "ERROR for $Url ($uri): Exception: $($_.Exception.Message)"
		}
		finally
		{
			# Finally, we clean up the http request by closing it.
			if( $HTTP_Response -ne $null)
			{
				$HTTP_Response.Close()
			}
		}

		return $result
	}
}

Export-ModuleMember -Function Test-WebResponse




